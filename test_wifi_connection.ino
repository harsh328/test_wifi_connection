#include <WiFiEsp.h>

#ifndef HAVE_HWSERIAL1
#include "SoftwareSerial.h"
  SoftwareSerial Serial1(6, 7); // RX, TX
#endif

const char ssid[] = "AQI";
const char pass[] = "12345678";

String ussid = "";
String upass = "";

int status = WL_IDLE_STATUS;
int reqCount = 0;

byte buffer[100];
String data = ""; //remove

WiFiEspServer server(80);

void setup()
{
  Serial.begin(9600);
  Serial1.begin(9600);    // initialize serial for ESP module
  WiFi.init(&Serial1);    // initialize ESP module

  // check for the presence of the shield
  if (WiFi.status() == WL_NO_SHIELD) {
    Serial.println("WiFi shield not present");
    while (true);
  }
  Serial.print("Attempting to start AP ");
  Serial.println(ssid);
  
  IPAddress localIp(192, 168, 1, 1);
  WiFi.configAP(localIp);
  status = WiFi.beginAP(ssid, 10, pass, ENC_TYPE_WPA2_PSK);// start access point

  Serial.println("Access point started");
  printWifiStatus();

  // start the web server on port 80
  server.begin();
  Serial.println("Server started");
  while (ussid == "") {
     WiFiEspClient client = server.available();  // listen for incoming clients
     if (client) {
       int ctr = 0;
       buffer[100] = "";
       // an http request ends with a blank line
       boolean currentLineIsBlank = true;
       while (client.connected()) {
         while (client.available()) {
           char c = client.read();
            // if you've gotten to the end of the line (received a newline
            // character) and the line is blank, the http request has ended,
            // so you can send a reply
           if (c == '\n' && currentLineIsBlank) {
              // Here is where the POST data is.
             Serial.print("Post data: ");
             
             while (client.available()){ //remove the loop
                buffer[ctr] = client.read();
                data =+ char(buffer[ctr]);
                ctr++;
             }
             
             Serial.println(data);
             
             int datactr = 0;
             int andctr = 0;
             int userctr = 0;
             int passctr = 32;
             for (int x = 0; x < ctr; x++) {
               if (buffer[x] == '=') {
                 datactr++;
                 x++;
                 Serial.println("User: ");
               }
                
               if (buffer[x] == '&') {
                 andctr++;
                 x++;
                 Serial.println("Password: ");
               }

               if (datactr == 1 && andctr == 0) {
                  Serial.write(buffer[x]);
                  userctr++; //try to remove this
                  ussid+=char(buffer[x]);
               }

               if (datactr == 2 && andctr == 1) {
                  Serial.write(buffer[x]);
                  passctr++; //try to remove this
                  upass+=char(buffer[x]);
               }
             }
              
             Serial.print("Buffer: ");
              
             Serial.println();
              
             client.println("HTTP/1.0 200 OK");
             client.println("Content-Type: text/html");
             client.println("Connection: keep-alive");
             client.println();
             
             client.println();
             client.println("<!DOCTYPE html>");
             client.println("<html lang=\"en\">");
             client.println("<body>");
             client.println(" <FORM action=\"\" method=\"POST\">");
             client.println("    <P>");
             client.println("    <LABEL for=\"username\">SSID:</LABEL>");
             client.println("    <INPUT type=\"text\" name=\"uname\"><BR><BR>");
             client.println("    <LABEL for=\"password\">Password:</LABEL>");
             client.println("    <INPUT type=\"password\" name=\"pwd\"><BR><BR>");
             client.println("    <INPUT type=\"submit\" value=\"Submit\">");
             client.println("    </P>");
             client.println(" </FORM>");
             
             if (ussid != "") {
               client.println(" <p>You have succesfully changed your SSID and password</p> ");
             }
             
             client.println("</body>");
             client.println("</html>");
             client.println();
             client.stop();
           }
           else if (c == '\n') {
             currentLineIsBlank = true;
           }
           else if (c != '\r') {
             currentLineIsBlank = false;
           }
         }
       }
       Serial.println();
     }
   }
   ussid=urldecode(ussid);
   upass=urldecode(upass);
   Serial.println(ussid); //remove
   Serial.println(upass); //remove

  char ussidArr[ussid.length()+1];
  char upassArr[upass.length()+1];

  ussid.toCharArray(ussidArr,ussid.length()+1); //declare variable for length
  upass.toCharArray(upassArr,upass.length()+1); //declare variable for length

  WiFi.reset();
  status=WL_IDLE_STATUS;
  
  while ( status != WL_CONNECTED) {
    Serial.print("Attempting to connect to WPA SSID: "); //remove
    Serial.println(ussid); //remove
    // Connect to WPA/WPA2 network
    status = WiFi.begin(ussidArr, upassArr);
  }
  Serial.println("You're connected to the network"); //remove
}

void loop(){
  Serial.println("You're now in the loop");
}

String urldecode(String str){
    String encodedString="";
    char c;
    char code0;
    char code1;
    for (int i =0; i < str.length(); i++){
        c=str.charAt(i);
      if (c == '+'){
        encodedString+=' ';  
      }else if (c == '%') {
        i++;
        code0=str.charAt(i);
        i++;
        code1=str.charAt(i);
        c = (h2int(code0) << 4) | h2int(code1);
        encodedString+=c;
      } else{
        encodedString+=c;  
      }
    }
    return encodedString;
}

unsigned char h2int(char c)
{
    if (c >= '0' && c <='9'){
        return((unsigned char)c - '0');
    }
    if (c >= 'a' && c <='f'){
        return((unsigned char)c - 'a' + 10);
    }
    if (c >= 'A' && c <='F'){
        return((unsigned char)c - 'A' + 10);
    }
    return(0);
}

void printWifiStatus(){
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);
  Serial.println();
  Serial.print("To see this page in action, connect to ");
  Serial.print(ssid);
  Serial.print(" and open a browser to http://");
  Serial.println(ip);
  Serial.println();
}
